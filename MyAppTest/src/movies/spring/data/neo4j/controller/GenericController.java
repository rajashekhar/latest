package movies.spring.data.neo4j.controller;

import org.springframework.web.bind.annotation.RequestMapping;

import movies.spring.data.neo4j.services.Service;

@RequestMapping("/")
public abstract class GenericController<T> {
	public T create(T entity) {
		return getService().create(entity);
	}

	public abstract Service<T> getService();
}
