package movies.spring.data.neo4j.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import movies.spring.data.neo4j.domain.Client;
import movies.spring.data.neo4j.services.ClientService;
import movies.spring.data.neo4j.services.Service;

@RestController
@RequestMapping("/clients")
public class ClientController extends GenericController<Client> {

	@Autowired
	private ClientService clientService;

	@PostMapping
	public Client save(@RequestBody Client client) {
		return super.create(new Client(client.getName()));
	}

	@Override
	public Service<Client> getService() {
		return clientService;
	}
}
