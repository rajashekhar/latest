package movies.spring.data.neo4j.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import movies.spring.data.neo4j.domain.Department;
import movies.spring.data.neo4j.services.DepartmentService;
import movies.spring.data.neo4j.services.Service;

@RestController
@RequestMapping("/departments")
public class DepartmentController extends GenericController<Department> {

	@Autowired
	private DepartmentService departmentService;

	@PostMapping
	public Department save(@RequestBody Department dept) {
		return super.create(new Department(dept.getEmail()));
	}

	@Override
	public Service<Department> getService() {
		return departmentService;
	}

}
