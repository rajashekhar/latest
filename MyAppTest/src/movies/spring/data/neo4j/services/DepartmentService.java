package movies.spring.data.neo4j.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Service;

import movies.spring.data.neo4j.domain.Department;
import movies.spring.data.neo4j.repositories.DepartmentRepository;

@Service
public class DepartmentService extends GenericService<Department> {

	@Autowired
	private DepartmentRepository departmentRepository;

	@Override
	public Neo4jRepository<Department, Long> getRepository() {
		return departmentRepository;
	}

}
