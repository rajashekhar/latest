package movies.spring.data.neo4j.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Service;

import movies.spring.data.neo4j.domain.Client;
import movies.spring.data.neo4j.repositories.ClientRepository;

@Service
public class ClientService extends GenericService<Client> {

	@Autowired
	private ClientRepository clientRepository;

	@Override
	public Neo4jRepository<Client, Long> getRepository() {
		return clientRepository;
	}

}
