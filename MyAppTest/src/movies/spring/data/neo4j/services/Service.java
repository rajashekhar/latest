package movies.spring.data.neo4j.services;

public interface Service<T> {

	T create(T object);
}
