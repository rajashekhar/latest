package movies.spring.data.neo4j.services;

import org.springframework.data.neo4j.repository.Neo4jRepository;

public abstract class GenericService<T> implements Service<T> {

	@Override
	public T create(T object) {
		return getRepository().save(object);
	}

	public abstract Neo4jRepository<T, Long> getRepository();
}
