package movies.spring.data.neo4j.repositories;

import movies.spring.data.neo4j.domain.Client;

public interface ClientRepository extends GenericRepository<Client> {

}
