package movies.spring.data.neo4j.repositories;

import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface GenericRepository<T> extends Neo4jRepository<T, Long> {

}
