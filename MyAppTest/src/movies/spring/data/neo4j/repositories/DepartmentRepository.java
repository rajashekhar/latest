package movies.spring.data.neo4j.repositories;

import movies.spring.data.neo4j.domain.Department;

public interface DepartmentRepository extends GenericRepository<Department> {

}
