package movies.spring.data.neo4j.domain;

import java.util.UUID;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.typeconversion.Convert;
import org.neo4j.ogm.typeconversion.UuidStringConverter;

@NodeEntity
public class Department extends BasicNode<Department> {
	@Convert(UuidStringConverter.class)
	private UUID uniqueId;

	private String email;

	public Department() {

	}

	public Department(String email) {
		super();
		this.setId(null);
		this.uniqueId = UUID.randomUUID();
		this.email = email;
	}

	public UUID getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(UUID uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
