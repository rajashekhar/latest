package movies.spring.data.neo4j.domain;

import org.neo4j.ogm.annotation.GraphId;

public class BasicNode<T> {

	@GraphId
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
}
