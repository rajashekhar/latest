package movies.spring.data.neo4j.domain;

import java.util.UUID;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.typeconversion.Convert;
import org.neo4j.ogm.typeconversion.UuidStringConverter;

@NodeEntity
public class Client extends BasicNode<Client> {

	@Convert(UuidStringConverter.class)
	private UUID uniqueId;

	private String name;

	public Client() {

	}

	public Client(String name) {
		super();
		this.setId(null);
		this.uniqueId = UUID.randomUUID();
		this.name = name;
	}

	public UUID getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(UUID uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
